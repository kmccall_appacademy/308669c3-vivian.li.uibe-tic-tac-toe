class ComputerPlayer
  attr_reader :name, :board, :mark, :board

  def initialize(name, symbol = [])
    @mark = symbol
    @name = name
  end

  def mark=(mark)
    @mark = mark
  end

  def display(board)
    @board = board
  end

  def get_move
    if winning_move.nil?
      pos = available[rand(available.length-1)]
    else
      pos = winning_move
    end
    pos
  end

  def available
    spot = []
    @board.grid.each_with_index do |row, row_idx|
      row.each_index do |col_idx|
        spot << [row_idx, col_idx] if @board.grid[row_idx][col_idx].nil?
      end
    end
    spot
  end

  def winning_move
    len = @board.grid.length
    # row direction
    @board.grid.each_with_index do |row, idx|
      row.each_index do |col_idx|
        return [idx, row.index(nil)] if row[col_idx].nil? && row.count(@mark)==len-1
      end
    end

    # column direction
    @board.grid.transpose.each_with_index do |row, idx|
      row.each_index do |col_idx|
        return [col_idx, idx] if row[col_idx].nil? && row.count(@mark)==len-1
      end
    end

    # left diagonal
    cnt = 0
    nil_cnt = []
    (0...len).each do |idx|
      cnt += 1 if @board.grid[idx][idx] == @mark
      nil_cnt << [idx, idx] if @board.grid[idx][idx].nil?
    end
    return nil_cnt[0] if cnt == len - 1 && nil_cnt.count == 1

    # right diagonal
    cnt = 0
    nil_cnt = []
    (0...len).each do |idx|
      cnt += 1 if @board.grid[idx][len - 1 - idx] == @mark
      nil_cnt << [idx, len - 1 - idx] if @board.grid[idx][len-1-idx] == nil
    end
    return nil_cnt[0] if cnt == len - 1 && nil_cnt.count == 1
    nil
  end

end
