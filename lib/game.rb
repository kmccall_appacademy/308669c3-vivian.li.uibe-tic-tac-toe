require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :mark
  def initialize(player_one, player_two)
    @mark = mark
    @board = Board.new
    @player_one = player_one
    @player_two = player_two
  end

  def current_player
    @current_player= @player_one

  end

  def switch_players!
    @player_one, @player_two = @player_two, @player_one
  end

  def play_turn
    pos = current_player.get_move
    @board.place_mark(pos, current_player.mark)
    # @player_one.board.place_mark(pos, current_player.mark)
    # @player_two.board.place_mark(pos, current_player.mark)
    switch_players!
  end

  def play
  end

end
