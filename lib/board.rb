class Board
  attr_accessor :grid
  def initialize(grid = Array.new(3) { Array.new(3, nil) })
    @grid = grid
  end

  def place_mark(pos, symbol)
    x, y = pos
    @grid[x][y] = symbol
  end

  def empty?(pos)
    x, y = pos
    @grid[x][y].nil?
  end

  def winner
    # on a row
    @grid.each do |row|
      return row[0] if row.all? { |i| i == row[0] } && row[0]
    end

    # on a column
    @grid.transpose.each do |col|
      return col[0] if col.all? { |i| i == col[0] } && col[0]
    end

    # left diagonal
    cnt_x = 0
    cnt_y = 0
    (0...@grid.length).each do |x|
      cnt_x += 1 if @grid[x][x] == :X
      cnt_y += 1 if @grid[x][x] == :O
    end

    return :X if cnt_x == @grid.length
    return :O if cnt_y == @grid.length

    # right diagonal
    cnt_x = 0
    cnt_y = 0
    len = @grid.length
    (0...len).each do |x|
      cnt_x += 1 if @grid[x][len - x - 1] == :X
      cnt_y += 1 if @grid[x][len - x - 1] == :O
    end
    return :X if cnt_x == len
    return :O if cnt_y == len
    nil
  end

  def over?
    return true unless winner.nil?
    @grid.each do |row|
      return false if row.any?(&:nil?)
    end
    true
  end

end
